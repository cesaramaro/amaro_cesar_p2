package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SortedCircularDoublyLinkedList<E extends Comparable<E>> implements SortedList<E> {

	private int currentSize;
	private Node<E> header;

	public SortedCircularDoublyLinkedList() {
		this.currentSize = 0;
		this.header = new Node<E>();
	}

	/**
	 * Returns an iterator, starting from the first element of the list
	 * @return Iterator<E>
	 */
	@Override
	public Iterator<E> iterator() {
		return new SortedCircularDoublyLinkedListIterator<E>();
	}

	/**
	 * Adds an element before the specified node
	 * @param obj - Object to add
	 * @param node - Node after the new element
	 */
	private void addBefore(E obj, Node<E> node) {
		Node<E> newNode = new Node<E>(obj, node.getPrev(), node);
		node.getPrev().setNext(newNode);
		node.setPrev(newNode);
		currentSize++;
	}

	/**
	 * Adds an element after the specified node
	 * @param obj - Object to add
	 * @param node - Node before the new element
	 */
	private void addAfter(E obj, Node<E> node) {
		Node<E> newNode = new Node<E>(obj, node, node.getNext());
		node.setNext(newNode);
		newNode.getNext().setPrev(newNode);
		currentSize++;
	}

	/**
	 * Adds an element after the specified node
	 * @param obj - Object to add
	 * @param node - Node before the new element
	 */
	@Override
	public boolean add(E obj) { 
		Node<E> newNode;
		if (isEmpty()) {
			newNode = new Node<E>(obj, header, header);
			header.setNext(newNode);
			header.setPrev(newNode);
			currentSize++;
			return true;
		} else {
			Node<E> curr = header.getNext();
			while (curr != header) {
				if (obj.compareTo(curr.getData()) <= 0) {
					addBefore(obj, curr);
					return true;
				}
				curr = curr.getNext();
			}
			addAfter(obj, header.getPrev());
			return true;
		}
	}

	/**
	 * Returns the current size of the list
	 * @return int - size of the list
	 */
	@Override
	public int size() {
		return currentSize;
	}

	/**
	 * Removes the specified object from the list
	 * @param E object to remove
	 * @return boolean - whether it was removed
	 */
	@Override
	public boolean remove(E obj) {
		if (!contains(obj)) return false;
		Node<E> curr = header.getNext();

		while (curr != header) {
			if (curr.getData().equals(obj)) {
				removeNode(curr);
				curr = null;
				return true;
			} else curr = curr.getNext();
		}
		return false;
	}

	/**
	 * Removes the object in the specified index from the list
	 * @param int index of the object to remove
	 * @return boolean - whether it was removed
	 */
	@Override
	public boolean remove(int index) {
		if (index >= size() || index < 0) throw new IndexOutOfBoundsException();
		Node<E> curr = header.getNext();

		for (int i = 0; curr != header; i++) {
			if (i == index) {
				removeNode(curr);
				curr = null;
				return true;
			} else curr = curr.getNext();
		}
		return false;
	}

	/**
	 * Removes the specified node
	 * @param curr - node to remove 
	 */
	private void removeNode(Node<E> curr) {
		Node<E> prev = curr.getPrev();
		Node<E> next = curr.getNext();
		prev.setNext(next); 
		next.setPrev(prev);
		curr.setNext(null); 
		curr.setPrev(null);
		curr.setData(null);
		currentSize--;
	}

	/**
	 * Removes all copies of the specified element
	 * @param E obj - object to remove 
	 * @return int - amount of objects removed
	 */
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while (contains(obj)) if (remove(obj)) count++;
		return count;
	}

	/**
	 * Gets the first object in the list
	 * @return E first object in the list
	 */
	@Override
	public E first() {
		if (isEmpty()) return null;
		return header.getNext().getData();
	}

	/**
	 * Gets the last object in the list
	 * @return E last object in the list
	 */
	@Override
	public E last() {
		if (isEmpty()) return null;
		return header.getPrev().getData();
	}

	/**
	 * Gets the object at the specified index
	 * @param int index
	 * @return E - object at the specified index
	 */
	@Override
	public E get(int index) {
		if (index >= size() || index < 0) throw new IndexOutOfBoundsException();
		Node<E> curr = header.getNext();

		for (int i = 0; curr != header; i++) {
			if (index == i) return curr.getData();
			curr = curr.getNext();
		}
		return null;
	}

	/**
	 * Gets the node at the specified index
	 * @param index
	 * @return Node<E> node in the list at that index
	 */
	private Node<E> getNode(int index) {
		if (index >= size() || index < 0) throw new IndexOutOfBoundsException();
		Node<E> curr = header.getNext();

		for (int i = 0; curr != header; i++) {
			if (index == i) return curr;
			curr = curr.getNext();
		}
		return null;
	}

	/**
	 * Empties the list
	 */
	@Override
	public void clear() {
		while (!isEmpty()) remove(0);
	}

	/**
	 * Checks if the specified object is in the list
	 * @param E e - object to be searched
	 * @return boolean - whether the list contains the object
	 */
	@Override
	public boolean contains(E e) {
		if (isEmpty()) return false;
		Node<E> curr = header.getNext();

		while (curr != header) {
			if (curr.getData().equals(e)) return true;
			curr = curr.getNext();
		}
		return false;
	}

	/**
	 * Checks if the list is empty
	 * @return boolean
	 */
	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Returns an iterator, starting from the specified index
	 * @param int - index from which to start the iterator
	 * @return Iterator<E>
	 */
	@Override
	public Iterator<E> iterator(int index) {
		if (index >= size() || index < 0) throw new IndexOutOfBoundsException("Invalid Index");
		return new SortedCircularDoublyLinkedListIterator<E>(index);
	}

	/**
	 * Gets the first index in the list of the specified object
	 * @param E object to search
	 * @return int - index of the first position of the object
	 */
	@Override
	public int firstIndex(E e) {
		if (isEmpty() || !contains(e)) return -1;
		Node<E> curr = header.getNext();

		for (int i = 0; curr != header; i++) {
			if (curr.getData().equals(e)) return i;
			curr = curr.getNext();
		}
		return -1;
	}

	/**
	 * Gets the last index in the list of the specified object
	 * @param E object to search
	 * @return int - index of the last position of the object
	 */
	@Override
	public int lastIndex(E e) {
		if (isEmpty() || !contains(e)) return -1;
		Node<E> curr = header.getPrev();

		for (int i = size()-1; curr != header; i++) {
			if (curr.getData().equals(e)) return i;
			curr = curr.getPrev();
		}
		return -1;
	}

	/**
	 * Returns a reverse iterator, starting from the last element in the list
	 * @return ReverseIterator<E>
	 */
	@Override
	public ReverseIterator<E> reverseIterator() {
		return new SortedCircularDoublyLinkedListReverseIterator<E>();
	}

	/**
	 * Returns a reverse iterator, starting from the specified index
	 * @param int - index from which to start the iterator
	 * @return ReverseIterator<E>
	 */
	@Override
	public ReverseIterator<E> reverseIterator(int index) {
		if (index >= size() || index < 0) throw new IndexOutOfBoundsException("Invalid Index");
		return new SortedCircularDoublyLinkedListReverseIterator<E>(index);
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	private class SortedCircularDoublyLinkedListReverseIterator<E> implements ReverseIterator<E> {

		private Node<E> currentNode;
		private int size = size();

		public SortedCircularDoublyLinkedListReverseIterator(int index) {
			this.currentNode = (Node<E>) getNode(index);
		}

		public SortedCircularDoublyLinkedListReverseIterator() {
			this.currentNode = (Node<E>) header.getPrev();
		}

		@Override
		public boolean hasPrevious() {
			return size > 0;
		}

		@Override
		public E previous() {
			E toReturn = null;
			if (!hasPrevious()) throw new NoSuchElementException();
			else {
				if (currentNode == header) currentNode = currentNode.getPrev();
				toReturn = currentNode.getData();
				currentNode = currentNode.getPrev();
				size--;
			}
			return toReturn;
		}
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	private class SortedCircularDoublyLinkedListIterator<E> implements Iterator<E> {

		private Node<E> currentNode;
		private int size = size();

		public SortedCircularDoublyLinkedListIterator(int index) {
			this.currentNode = (Node<E>) getNode(index);
		}

		public SortedCircularDoublyLinkedListIterator() {
			this.currentNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return size > 0;
		}

		@Override
		public E next() {
			E toReturn = null;
			if (!hasNext()) throw new NoSuchElementException();
			else {
				if (currentNode == header) currentNode = currentNode.getNext();
				toReturn = currentNode.getData();
				currentNode = currentNode.getNext();
				size--;
			}
			return toReturn;
		}
	}

	private static class Node<E> {

		private E data;
		private Node<E> next, prev;

		public Node(E data, Node<E> prev, Node<E> next) {
			super();
			this.data = data;
			this.prev = prev;
			this.next = next;
		}

		public Node() { 
			this(null, null, null); 
		}

		public E getData() {
			return data; 
		}

		public void setData(E element) {
			this.data = element; 
		}

		public Node<E> getNext() { 
			return next;
		}

		public void setNext(Node<E> next) { 
			this.next = next; 
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> previous) {
			this.prev = previous;
		}
	}
}
