package edu.uprm.ece.icom4035.list;

public class ListTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SortedList<String> theList = new SortedCircularDoublyLinkedList<String>();
		SortedList<Integer> theList2 = new SortedCircularDoublyLinkedList<Integer>();
		theList2.add(6);
		theList2.add(5);
		theList2.add(4);
		theList2.add(3);
		theList2.add(7);
		theList2.add(3);
		theList2.add(15);
		theList2.add(2);
		theList2.add(1);
		theList2.add(0);
		theList2.add(0);
		theList2.add(7);
		printList2(theList2);
		System.out.println(" ");
		theList2.removeAll(0);
		printList2(theList2);
		System.out.println("Empty List: " + theList.isEmpty());
		theList.add("Bob");
		printList(theList);
		theList.add("Ron");
		printList(theList);
		theList.add("Jil");
		printList(theList);
		System.out.println("Element 0 is Bob: " + theList.get(0).equals("Bob"));
		System.out.println("Element 1 is Jil: " + theList.get(1).equals("Jil"));
		theList.add("Amy");
		System.out.println("First element is Amy: " + theList.first().equals("Amy"));
		theList.add("Ned");
		printList(theList);
		System.out.println("Last element is Ned: " + theList.last().equals("Ned"));  // must be false
		System.out.println("Last element is Ron: " + theList.last().equals("Ron"));
		theList.add("Cal");
		printReverseList(theList);
		System.out.println("Remove element at position 1: " + theList.remove(1));
		printReverseList(theList);
		System.out.println("Remove last elements: " + theList.remove("Ron"));
		printList(theList);
		System.out.println();
		printReverseList(theList);
		theList2.clear();
		printList2(theList2);
		System.out.println("Reverse Iterator from index 2 (" + theList.get(2) + ")" );
		printReverseListIndex(theList, 2);
		
	}

	private static void printList(SortedList<String> theList) {
		for (String s: theList) {
			System.out.println(s);
		}
	}
	
	private static void printList2(SortedList<Integer> theList) {
		for (Integer s: theList) {
			System.out.println(s);
		}
	}

	private static void printReverseList(SortedList<String> theList) {
		for (ReverseIterator<String> iter = theList.reverseIterator(); iter.hasPrevious();) {
			System.out.println(iter.previous());
		}
	}
	
	private static void printReverseListIndex(SortedList<String> theList, int index) {
		for (ReverseIterator<String> iter = theList.reverseIterator(index); iter.hasPrevious();) {
			System.out.println(iter.previous());
		}
	}
}
